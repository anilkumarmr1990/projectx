const express = require("express");
const moment = require("moment");
let auth = express.Router();
let db = require("../db/db");

auth.use(function (req, res, next) {
   console.log(req.url, "@", Date.now());
   next();
});

auth.post("/", (req, res) => {
   let { phone } = req.body;
   db.query(
      "SELECT id from users where phone=? and deleted_on is null",
      [phone],
      function (err, rows) {
         if (err) {
            res.status(500).send({ message: err.sqlMessage });
            throw err;
         } else {
            if (rows.length === 0) {
               var otp = Math.floor(1000 + Math.random() * 9000);
               db.query(
                  `insert into signup_requests(phone,time,otp) values(?,?,?)`,
                  [phone, moment().format("YYYY-MM-DD HH:mm:ss"), otp]
               );
               res.status(200).send({ type: "signup" });
            } else {
               var otp = Math.floor(1000 + Math.random() * 9000);
               db.query(
                  `insert into user_otp(user_id,phone,otp,time) values(?,?,?,?)`,
                  [
                     rows[0].id,
                     phone,
                     otp,
                     moment().format("YYYY-MM-DD HH:mm:ss"),
                  ]
               );
               res.status(200).send({ type: "login" });
            }
         }
      }
   );
});

auth.post("/signup", (req, res) => {
   const { phone, otp } = req.body;
   db.query(
      "SELECT otp,time FROM projectX.signup_requests where phone=? order by time desc limit 1",
      [phone, moment().format("YYYY-MM-DD HH:mm:ss")],
      function (err, rows) {
         if (err) {
            res.status(500).send({ message: err.sqlMessage });
            throw err;
         } else {
            if (rows.length === 1 && otp === rows[0].otp) {
               db.query(
                  `insert into users(phone,status,created_on,updated_on) values(?,?,?,?)`,
                  [
                     phone,
                     0,
                     moment().format("YYYY-MM-DD HH:mm:ss"),
                     moment().format("YYYY-MM-DD HH:mm:ss"),
                  ]
               );
               res.status(200).send({ type: "Signed up successfully" });
            } else {
               res.status(401).send({
                  message: "Session timed out..Please try again",
               });
            }
         }
      }
   );
});

module.exports = auth;

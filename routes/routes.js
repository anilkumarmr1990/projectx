const express = require("express");
const routes = express.Router();

const auth = require("./auth");
const dikes = require("./dikes");
const profile = require("./profile");

routes.use("/auth", auth);
routes.use("/dikes", dikes);
routes.use("/profile", profile);

module.exports = routes;
